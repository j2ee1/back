package tn.iit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import tn.iit.services.CompteService;

@SpringBootApplication
public class FirstSpringProjectApplication implements CommandLineRunner  {
    @Autowired
    private CompteService compteService;

    public static void main(String[] args) {
        SpringApplication.run (FirstSpringProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        Compte compte = new Compte (new BigDecimal ("100000"), "Mseddi");
//        compteService.save (compte);
//        System.out.println ("FINISH");

    }
}

package tn.iit.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.iit.models.Client;
import tn.iit.repositories.ClientRepository;
import tn.iit.repositories.CompteRepository;
import tn.iit.repositories.UserRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ClientService {

    @Autowired
    private final ClientRepository clientRepository;
    @Autowired
    private final CompteRepository compteRepository;
    @Autowired
    private final UserRepository userRepository;

    public ClientService(ClientRepository clientRepository, CompteRepository compteRepository, UserRepository userRepository) {
        this.clientRepository = clientRepository;
        this.compteRepository = compteRepository;
        this.userRepository = userRepository;
    }

    public void save(Client client) {
        this.clientRepository.saveAndFlush(client);
    }

    public Client getById(Long id) {
        return this.clientRepository.getOne (id);
    }

    public boolean exists(Long id) {
        return this.clientRepository.existsById (id);
    }

    @Transactional
    public void delete(Long id) {
        this.compteRepository.deleteComptesByClient_Id(id);
        this.userRepository.deleteByClient_Id (id);
        this.clientRepository.deleteById (id);
    }

    public List<Client> getAll() {
        return this.clientRepository.findAll ();
    }

}

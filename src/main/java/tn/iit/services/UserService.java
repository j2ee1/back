package tn.iit.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.iit.models.Compte;
import tn.iit.models.User;
import tn.iit.repositories.UserRepository;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(User user) {
        userRepository.saveAndFlush (user);
    }

    public User getById(Long id) {
        return userRepository.getOne (id);
    }

    public boolean exists(Long id) {
        return userRepository.existsById (id);
    }

    public void delete(Long id) {
        userRepository.deleteById (id);
    }

    public List<User> getAll() {
        return userRepository.findAll ();
    }

    public boolean existByUsername(String username){
        return userRepository.existsByUsername (username);
    }

}

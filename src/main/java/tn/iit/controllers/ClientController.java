package tn.iit.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tn.iit.dto.ClientDto;
import tn.iit.models.Client;
import tn.iit.models.User;
import tn.iit.repositories.UserRepository;
import tn.iit.services.ClientService;

import java.util.List;

@CrossOrigin("*")
@RequestMapping(value = "/api/v1/clients")
@Controller
@RestController()
public class ClientController {
    private final ClientService clientService;
    private final UserRepository userRepository;

    public ClientController(ClientService clientService, UserRepository userRepository) {
        this.clientService = clientService;
        this.userRepository = userRepository;
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @PostMapping("/add")
    public ResponseEntity<?> AddClient(@RequestBody ClientDto clientDto) {
        Client client=new Client (clientDto.getFirstName (),clientDto.getLastName (),clientDto.getPhone (),clientDto.getAdress ());
        clientService.save (client);
        return ResponseEntity.ok ("Client has been created successfully");
    }

    @PreAuthorize ("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getClientById(@PathVariable(value = "id") Long id) {
        if (!clientService.exists (id)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("Client doesn't exist ");
        }
        return ResponseEntity.ok(clientService.getById (id));
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @GetMapping("/all")
    public ResponseEntity<List<Client>> getAll() { return ResponseEntity.ok (clientService.getAll ());
    }

    @PreAuthorize ("hasRole('USER')")
    @GetMapping("/user/{id}")
    public ResponseEntity<?> getClientByUserId(@PathVariable("id") Long id) {
        if(!this.userRepository.existsById (id))
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("User doesn't exist ");
        User user=this.userRepository.getOne (id);
        return ResponseEntity.ok (user.getClient ());
    }

    @PreAuthorize ("hasAnyRole('ADMIN','USER')")
    @PutMapping("/{id}")
    public ResponseEntity<?> updateClient(@PathVariable(value = "id") Long id, @RequestBody ClientDto clientDto) {
        if (!clientService.exists (id)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("Client doesn't exist ");
        }
        Client client=clientService.getById (id);
        client.setFirstName (clientDto.getFirstName ());
        client.setLastName (clientDto.getLastName ());
        client.setPhone (clientDto.getPhone ());
        client.setAdress (clientDto.getAdress ());
        clientService.save (client);
        return ResponseEntity.ok ("Client has been updated successfully");
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteClient(@PathVariable(value = "id") Long id) {
        if (!clientService.exists (id)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("Client doesn't exist ");
        }
        clientService.delete (id);
        return ResponseEntity.ok ("Client has been deleted successfully");
    }
}

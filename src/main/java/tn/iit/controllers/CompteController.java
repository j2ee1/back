package tn.iit.controllers;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tn.iit.dto.CompteClientDto;
import tn.iit.dto.CompteDto;
import tn.iit.models.Compte;
import tn.iit.security.services.UserDetailsImpl;
import tn.iit.services.ClientService;
import tn.iit.services.CompteService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin("*")
@RequestMapping(value = "/api/v1/comptes")
@Controller
@RestController()
public class CompteController {
    private final CompteService compteService;
    private final ClientService clientService;

    public CompteController(CompteService compteService, ClientService clientService) {
        this.compteService = compteService;
        this.clientService = clientService;
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @PostMapping("/add")
    public ResponseEntity<?> AddCompte(@RequestBody CompteDto compteDto) {
        Compte compte=new Compte (new BigDecimal (compteDto.getSolde ()),compteDto.getClient ());
        compteService.save (compte);
        return ResponseEntity.ok ("Account has been created successfully");
    }

    @PreAuthorize ("hasAnyRole('ADMIN','USER')")
    @GetMapping("/client/{id}")
    public ResponseEntity<?> getComptesByClientId(@PathVariable(name = "id") Long id){
        if (!clientService.exists (id)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("Client doesn't exist ");
        }

        List<Compte> results=compteService.getByClientId (id);
        List<CompteClientDto> comptesClient= new ArrayList<> ();
        for(Compte compte:results)
        {
            comptesClient.add (new CompteClientDto (compte.getRib ().toString (),compte.getSolde ().toString ()));
        }
        return ResponseEntity.ok (comptesClient);
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @GetMapping("/{rib}")
    public ResponseEntity<?> getCompteByRib(@PathVariable(value = "rib") Long rib) {
        if (!compteService.exists (rib)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("Account doesn't exist ");
        }
        return ResponseEntity.ok(compteService.getByRib (rib));
    }


    @PreAuthorize ("hasRole('ADMIN')")
    @GetMapping("/all")
    public ResponseEntity<List<Compte>> getAll() { return ResponseEntity.ok (compteService.getAll ()); }


    @PreAuthorize ("hasRole('ADMIN')")
    @PutMapping("/{rib}")
    public ResponseEntity<?> updateCompte(@PathVariable(value = "rib") Long rib, @RequestBody CompteDto compteDto) {
        if (!compteService.exists (rib)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("Account doesn't exist ");
        }
        Compte compte=compteService.getByRib (rib);
        compte.setSolde (new BigDecimal (compteDto.getSolde ()));
        compteService.save (compte);
        return ResponseEntity.status (HttpStatus.CREATED).body ("Account has been updated successfully");
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @DeleteMapping("/{rib}")
    public ResponseEntity<?> deleteCompte(@PathVariable(value = "rib") Long rib) {
        if (!compteService.exists (rib)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("Account doesn't exist ");
        }
        compteService.delete (rib);
        return ResponseEntity.ok ("Account has been deleted successfully");
    }
}

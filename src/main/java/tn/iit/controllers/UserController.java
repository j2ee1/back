package tn.iit.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tn.iit.dto.UserDto;
import tn.iit.models.ERole;
import tn.iit.models.Role;
import tn.iit.models.User;
import tn.iit.repositories.RoleRepository;
import tn.iit.services.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin("*")
@RequestMapping(value = "/api/v1/users")
@Controller
@RestController()
public class UserController {

    @Autowired
    private final RoleRepository roleRepository;

    @Autowired
    private final UserService userService;

    @Autowired
    private final PasswordEncoder encoder;

    public UserController(RoleRepository roleRepository, UserService userService,
                          PasswordEncoder encoder) {
        this.roleRepository = roleRepository;
        this.userService = userService;
        this.encoder = encoder;
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @PostMapping("/add")
    public ResponseEntity<?> AddUser(@RequestBody UserDto userDto) {
       if(userService.existByUsername (userDto.getUsername ()))
       {
           return ResponseEntity
                   .badRequest ().body ("Username already exist ");
       }
        User user=new User (userDto.getUsername (),userDto.getEmail (),encoder.encode (userDto.getPassword ()),userDto.getClient ());
        Set<Role> roles = new HashSet<> ();
        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        roles.add(userRole);
        user.setRoles(roles);
        userService.save (user);
        return ResponseEntity.ok ("User has been created successfully");
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable(value = "id") Long id) {
        if (!userService.exists (id)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("User doesn't exist ");
        }
        return ResponseEntity.ok(userService.getById (id));
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @GetMapping("/all")
    public ResponseEntity<List<User>> getAll() { return ResponseEntity.ok (userService.getAll ());
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@PathVariable(value = "id") Long id, @RequestBody UserDto userDto) {
        if (!userService.exists (id)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("User doesn't exist ");
        }
        User user=userService.getById (id);
        user.setUsername (userDto.getUsername ());
        user.setEmail (userDto.getEmail ());
        user.setPassword (encoder.encode (userDto.getPassword ()));
        userService.save (user);
        return ResponseEntity.ok ("User has been updated successfully");
    }

    @PreAuthorize ("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long id) {
        if (!userService.exists (id)) {
            return ResponseEntity
                    .status (HttpStatus.NOT_FOUND).body ("User doesn't exist ");
        }
        userService.delete (id);
        return ResponseEntity.ok ("User has been deleted successfully");
    }
}

package tn.iit.dto;

import lombok.*;
import tn.iit.models.Client;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDto {

    private String username;
    private String email;
    private String password;
    private Client client;

}

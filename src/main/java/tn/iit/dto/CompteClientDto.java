package tn.iit.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CompteClientDto {
    private String rib;
    private String solde;
}
